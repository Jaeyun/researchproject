﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PivotScript : MonoBehaviour {
    [SerializeField] private RectTransform m_leftLeft;
    [SerializeField] private RectTransform m_rightRight;
    [SerializeField] private RectTransform m_leftMiddle;
    [SerializeField] private RectTransform m_rightMiddle;
    [SerializeField] private RectTransform m_Middle;

    public RectTransform LL { get { return m_leftLeft; } }
    public RectTransform RR { get { return m_rightRight; } }
    public RectTransform LM { get { return m_leftMiddle; } }
    public RectTransform RM { get { return m_rightMiddle; } }
    public RectTransform MM { get { return m_Middle; } }
}
