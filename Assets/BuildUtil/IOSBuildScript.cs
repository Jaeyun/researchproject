﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace KOFG.Custom.Build
{
#if UNITY_EDITOR
    public class IOSBuildScript : BuildScript
    {
        public override void BuildAssetBundle()
        {
            Debug.Log("IOSBuildScript.BuildAssetBundle メンテ中");
        }
        public override void BuildRom()
        {
            // TODO XCode設定などの調整を入れる必要あり。
            Debug.Log("IOSBuildScript.BuildRom メンテ中");
        }
    }
#endif
}