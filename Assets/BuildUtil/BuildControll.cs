﻿using UnityEditor;
namespace KOFG.Custom.Build
{
#if UNITY_EDITOR
    /// <summary>
    /// Unityのメニューに表示されるカスタマイズメニューボタンとそれぞれの挙動してのクラス
    /// </summary>
    public static class BuildControll
    {
        // ビルドタゲっと
        private static BuildTarget m_Target;
        private static BuildScript m_buildScript = null;

        /// <summary>
        /// 初期化メソッド(ビルドターゲットを判断し、OS毎のビルド定義クラスを生成)
        /// </summary>
        public static void Init()
        {
            // 現在のプラットフォームを基準としてビルドターゲットを指定
            m_Target = EditorUserBuildSettings.activeBuildTarget;
            if (m_Target == BuildTarget.Android)
            {
                m_buildScript = new AndroidBuildScript();
            }
            else if (m_Target == BuildTarget.iOS)
            {
                m_buildScript = new IOSBuildScript();
            }
            // その他のプラットフォームはエラーハンドリング
            else
            {
                EditorUtility.DisplayDialog("Error", "Android / iOS 以外のBuild Targetではビルドできません。", "確認");
            }
        }

        [MenuItem("CustomBuild/Build Full(Current Platform)")]
        /// <summary>
        /// 全体ビルド
        /// </summary>
        public static void BuildFull()
        {
            if (m_buildScript == null)
            {
                Init();
            }
            m_buildScript.BuildFull();
        }

        [MenuItem("CustomBuild/Build Rom Only(Current Platform)")]
        /// <summary>
        /// Romのみビルド
        /// </summary>
        public static void BuildRom()
        {
            if (m_buildScript == null)
            {
                Init();
            }
            m_buildScript.BuildRom();
        }

        [MenuItem("CustomBuild/Build AssetBundle Only(Current Platform)")]
        /// <summary>
        /// AssetBundleのみビルド
        /// </summary>
        public static void BuildAssetBundle()
        {
            if (m_buildScript == null)
            {
                Init();
            }
            m_buildScript.BuildRom();
        }
    }
#endif
}