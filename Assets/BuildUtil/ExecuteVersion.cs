﻿namespace KOFG.Custom.Build
{
    /// <summary>
    /// 運用段階でアプリ更新などをチェックする基準になるExecuteVersionの管理クラス
    /// </summary>
    public class ExecuteVersion
    {
        /// <value>
        /// *** Only リリース向け ***
        /// 画面表示などでアプリバージョンを取得する
        /// フォーマット：YYMMDDNN
        /// YY: リリース/配布ターゲットにする年度２桁
        /// MM: リリース/配布ターゲットにする月２桁
        /// DD: リリース/配布ターゲットにする日２桁
        /// NN: YYMMDDを基準として何番目のビルドか。
        /// </value>
        /// <remarks>
        /// この数値は必ずビルド担当者と関係者(エンジニア全員)の承認を受けること。
        /// Getのみ
        /// </remarks>
        public static int Ver
        {
            get
            {
                return 18082700;
            }
        }

        /// <value>
        /// *** Only 制作向け ***
        /// 開発でのスプリントを取得する
        /// </value>
        /// <remarks>
        /// この数値は必ずビルド担当者と関係者(エンジニア全員)の承認を受けること。
        /// Getのみ
        /// </remarks>
        public static string Splint
        {
            get
            {
                return "ITE666";
            }
        }
    }
}
