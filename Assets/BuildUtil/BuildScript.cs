﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;

namespace KOFG.Custom.Build
{
#if UNITY_EDITOR
    public abstract class BuildScript
    {
        // 本体ファイルフォルダー名
        private readonly string m_strRomFileOutputRootPath = "RomFile";
        // BundleID名
        protected readonly string m_strIdentifier = "net.vegames.kofg";
        // プロジェクトフォルダーパス
        protected readonly string m_strRomfileOutputPath;
        // ビルドオプション
        protected readonly BuildOptions m_buildOption;

        /// <summary>
        /// コンストラクター
        /// </summary>
        protected BuildScript()
        {
            // ビルドオプション指定
            m_buildOption = EditorUserBuildSettings.development ? 
                                            BuildOptions.Development : BuildOptions.None;
            // RomFileの吐き出し先指定
            m_strRomfileOutputPath = Path.Combine(m_strRomFileOutputRootPath, 
                                                  EditorUserBuildSettings.activeBuildTarget.ToString());
        }

        /// <summary>
        /// 全体ビルド(ABビルド→Romビルド)
        /// </summary>
        public void BuildFull()
        {
            BuildAssetBundle();
            BuildRom();
        }
        /// <summary>
        /// OS毎AssetBundleビルドメソッド(多分、同じなと思うけど。。。)
        /// </summary>
        public abstract void BuildAssetBundle();
        /// <summary>
        /// Rom(APK, Xcode)ビルドメソッド
        /// </summary>
        public abstract void BuildRom();



        /// <summary>
        /// シーン個数チェック、文字列配列として返す
        /// </summary>
        /// <returns>シーン名前の配列</returns>
        protected static string[] GetLevelsFromBuildSettings()
        {
            List<string> levels = new List<string>();
            for (int i = 0; i < EditorBuildSettings.scenes.Length; ++i)
            {
                if (EditorBuildSettings.scenes[i].enabled)
                    levels.Add(EditorBuildSettings.scenes[i].path);
            }

            return levels.ToArray();
        }

        /// <summary>
        /// 吐き出される本体のファイル名を指定するメソッド
        /// </summary>
        /// <returns>本体の名前</returns>
        /// <param name="target">ビルドプラントフォーム</param>
        protected static string GetBuildTargetName(BuildTarget target)
        {
            var targetFormat = string.Empty;

            switch (target)
            {
                case BuildTarget.Android:
                    targetFormat = ".apk";
                    break;
                case BuildTarget.StandaloneWindows:
                case BuildTarget.StandaloneWindows64:
                    targetFormat = ".exe";
                    break;
                case BuildTarget.StandaloneOSXIntel:
                case BuildTarget.StandaloneOSXIntel64:
                case BuildTarget.StandaloneOSX:
                    targetFormat = ".app";
                    break;
                // Add more build targets for your own.
                default:
                    Debug.Log("Target not implemented.");
                    return null;
            }
            return $"KOFG_{ExecuteVersion.Splint}_{DateTime.Now.ToString("yyMMdd_HHmmss")}{targetFormat}";
        }
    }
#endif
}