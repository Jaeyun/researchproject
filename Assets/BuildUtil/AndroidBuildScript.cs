﻿using UnityEngine;
using UnityEditor;
namespace KOFG.Custom.Build
{
#if UNITY_EDITOR
    /// <summary>
    /// Androidビルドを担当するクラス
    /// </summary>
    public class AndroidBuildScript : BuildScript
    {
        /// <summary>
        /// Android向けアセットバンドルビルドメソッド
        /// </summary>
        public override void BuildAssetBundle()
        {
            Debug.Log("AndroidBuildScript.BuildAssetBundle");
            EditorUtility.DisplayDialog("AndroidBuildScript", "AndroidBuildScript.BuildAssetBundle", "メンテ中");
        }

        /// <summary>
        /// Android向けRomビルドスクリプト
        /// </summary>
        public override void BuildRom()
        {
            // 登録済みのシーンファイルを取得
            var levels = GetLevelsFromBuildSettings();
            if (levels.Length == 0)
            {
                Debug.LogError("Nothing Scene to build.");
                return;
            }

            // Romfile名を指定
            var targetName = GetBuildTargetName(EditorUserBuildSettings.activeBuildTarget);
            if (targetName == null) return;

            // Bundle Identifire 指定
            PlayerSettings.applicationIdentifier = m_strIdentifier;

            // 登録済みのシーンを含ませてビルドを開始
            BuildPipeline.BuildPlayer(levels, 
                                      m_strRomfileOutputPath + targetName, 
                                      EditorUserBuildSettings.activeBuildTarget, 
                                      m_buildOption);
        }
    }
#endif
}