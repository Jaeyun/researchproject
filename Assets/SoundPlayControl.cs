﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;
using UnityEngine.UI;

public class SoundPlayControl : MonoBehaviour {
    [SerializeField] private AudioSource m_audioSource;
    [SerializeField] private Text m_text;
    private readonly string MP3_PATH = "Sound/MP3_64kbps";
    private readonly string OGG_PATH = "Sound/OGG_64kbps";
    private Stopwatch sw = new Stopwatch();

    public void PlayMp3()
    {
        sw.Start();
        var snd = Resources.Load<AudioClip>(MP3_PATH);
        PlayProcess("MP3", snd);
    }

    public void PlayOGG()
    {
        sw.Start();
        var snd = Resources.Load<AudioClip>(OGG_PATH);
        PlayProcess("OGG", snd);
    }

    private void PlayProcess(string format, AudioClip clip)
    {
        m_audioSource.clip = clip;
        m_audioSource.Play();
        sw.Stop();
        m_text.text = $"{format} : {sw.ElapsedMilliseconds.ToString()}";
        sw.Reset();
    }
}
