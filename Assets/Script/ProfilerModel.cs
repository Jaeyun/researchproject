﻿using UnityEngine;
using UniRx;

public class ProfilerModel
{
    public IReactiveProperty<bool> IsProfilerActive { get;} = new ReactiveProperty<bool>();
    public IReactiveProperty<int> CharacterCount { get;} = new ReactiveProperty<int>();
    public IReactiveProperty<int> ParticleCount { get;} = new ReactiveProperty<int>();
    public IReactiveProperty<float> FPS { get; } = new ReactiveProperty<float>();

    public IReactiveProperty<bool> IsAbleCreateCharacter { get;} = new ReactiveProperty<bool>();
    public IReactiveProperty<bool> IsAbleCreateParticle { get; } = new ReactiveProperty<bool>();

    private static ProfilerModel m_inst = null;

    public static ProfilerModel Instance
    {
        get
        {
            if (m_inst == null)
            {
                m_inst = new ProfilerModel();
            }
            return m_inst;
        }
    }

    private ProfilerModel()
    {
        IsProfilerActive.Value = false;
        CharacterCount.Value = 0;
        ParticleCount.Value = 0;
        FPS.Value = 0f;

        IsAbleCreateCharacter.Value = true;
        IsAbleCreateParticle.Value = true;
    }

    public void Init()
    {
        CharacterCount.Value = 0;
        ParticleCount.Value = 0;
    }

    public void ProfilerOnOff()
    {
        IsProfilerActive.Value = !IsProfilerActive.Value;
    }

    public void IncreaseCharacterCount()
    {
        if (!IsAbleCreateCharacter.Value) return;

        CharacterCount.Value++;
        if (CharacterCount.Value >= 5)
        {
            IsAbleCreateCharacter.Value = false;
        }
        Debug.Log("Char : " + CharacterCount.Value);
    }

    public void IncreaseParticleCount()
    {
        if (!IsAbleCreateParticle.Value) return;

        ParticleCount.Value++;
        if (ParticleCount.Value >= 5)
        {
            IsAbleCreateParticle.Value = false;
        }
        Debug.Log("Particle : " + ParticleCount.Value);
    }

    public void SetCharacterCount(int val)
    {
        CharacterCount.Value = val;
        if (CharacterCount.Value < 5)
        {
            IsAbleCreateCharacter.Value = true;
        }
    }

    public void SetParticleCount(int val)
    {
        ParticleCount.Value = val;
        if (ParticleCount.Value < 5)
        {
            IsAbleCreateParticle.Value = true;
        }
    }

    public void SetFPSVal(float val)
    {
        FPS.Value = val;
    }
}
