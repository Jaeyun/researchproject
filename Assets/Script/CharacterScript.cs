﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class CharacterScript : MonoBehaviour
{
    [SerializeField] private Image m_body;
    [SerializeField] private Image m_emotion;

    private SpriteAtlas m_spriteAtlas;

    public void Init()
    {
        m_spriteAtlas = Resources.Load<SpriteAtlas>("SpriteAtlas/EmotionAtlas");
    }

    public void ChangeEmotion()
    {
        var Rnd = Random.Range(0, 100);
        var index = Rnd % 4 + 1;
        m_emotion.sprite = m_spriteAtlas.GetSprite(index.ToString());
    }
}
