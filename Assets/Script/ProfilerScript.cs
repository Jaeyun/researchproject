﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfilerScript : MonoBehaviour 
{
    [SerializeField] private Text m_countCharacter;
    [SerializeField] private Text m_countParticle;
    [SerializeField] private Text m_countFPS;

    public void Init()
    {
        m_countCharacter.text = string.Empty;
        m_countParticle.text = string.Empty;
        m_countFPS.text = string.Empty;
    }

    public void SetCharacterCount(int val)
    {
        m_countCharacter.text = val.ToString();
    }

    public void SetParticleCount(int val)
    {
        m_countParticle.text = val.ToString();
    }

    public void SetFPSValue(float val)
    {
        m_countFPS.text = val.ToString();
    }

    private void OnDestroy()
    {
        Debug.Log("ProfilerScript.Destroy()");
        m_countCharacter = null;
        m_countParticle = null;
        m_countFPS = null;
    }
}
