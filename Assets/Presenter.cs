﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

public class Presenter : MonoBehaviour {

    [SerializeField] private Button m_buttonParticle;
    [SerializeField] private Button m_buttonCharacter;
    [SerializeField] private Button m_buttonEmotion;
    [SerializeField] private Button m_buttonOnOff;
    [SerializeField] private ProfilerScript m_profilerScript;

    [SerializeField] private PivotScript m_pivotScript;

    [SerializeField] private Transform m_transformCharacter;
    [SerializeField] private Transform m_transformParticle;

    private List<CharacterScript> m_characterList;
    private List<GameObject> m_particletList;

    private void Awake()
    {
        if (m_buttonParticle == null ||
           m_buttonCharacter == null ||
           m_buttonParticle == null ||
           m_buttonOnOff == null)
        {
            Debug.LogError("Something is NULL");
            return;
        }

        m_characterList = new List<CharacterScript>();
        m_particletList = new List<GameObject>();
    }

    public void Init()
    {
        ProfilerModel.Instance.Init();

        m_buttonParticle.OnClickAsObservable()
                      .Subscribe(a => CreateEffect())
                      .AddTo(m_buttonParticle.gameObject)
                      .AddTo(gameObject);

        m_buttonCharacter.OnClickAsObservable()
                         .Subscribe(_ => CreateCharacter())
                         .AddTo(m_buttonCharacter.gameObject)
                         .AddTo(gameObject);

        m_buttonEmotion.OnClickAsObservable()
                       .Subscribe(c => ChangeEmotion())
                       .AddTo(m_buttonEmotion.gameObject)
                       .AddTo(gameObject);

        m_buttonOnOff.OnClickAsObservable()
                     .Subscribe(e => DeleteAllObject())
                     .AddTo(m_buttonOnOff.gameObject)
                     .AddTo(gameObject);

        this.FixedUpdateAsObservable()
            .Select(ev => m_profilerScript.gameObject.activeSelf)
            .RepeatUntilDestroy(gameObject)
            .Where(ev => ev)
            .Subscribe(_ => 
        {
            m_profilerScript.SetFPSValue(ProfilerModel.Instance.FPS.Value);
            m_profilerScript.SetParticleCount(ProfilerModel.Instance.ParticleCount.Value);
            m_profilerScript.SetCharacterCount(ProfilerModel.Instance.CharacterCount.Value);
        });
    }

    private void CreateCharacter()
    {
        if (!ProfilerModel.Instance.IsAbleCreateCharacter.Value) return;

        ProfilerModel.Instance.IncreaseCharacterCount();
        Debug.Log("CreateCharacter()");

        var nowVec = Vector3.zero;

        switch (ProfilerModel.Instance.CharacterCount.Value)
        {
            case 1:
                nowVec = m_pivotScript.MM.localPosition;
                break;
            case 2:
                nowVec = m_pivotScript.LM.localPosition;
                break;
            case 3:
                nowVec = m_pivotScript.RM.localPosition;
                break;
            case 4:
                nowVec = m_pivotScript.LL.localPosition;
                break;
            case 5:
                nowVec = m_pivotScript.RR.localPosition;
                break;
        }

        var characterPrefab = Resources.Load("Prefab/Character") as GameObject;
        var character = Instantiate(characterPrefab) as GameObject;

        character.transform.SetParent(m_transformCharacter);
        character.transform.localPosition = nowVec;
        character.transform.rotation = Quaternion.identity;
        character.transform.localScale = Vector3.one * 2;

        character.gameObject.name = $"Character{ProfilerModel.Instance.CharacterCount.Value + 1}";
        var characterScript = character.GetComponent<CharacterScript>();
        characterScript.Init();
        m_characterList.Add(characterScript);

        m_profilerScript.SetCharacterCount(ProfilerModel.Instance.CharacterCount.Value);

        if (!ProfilerModel.Instance.IsAbleCreateCharacter.Value)
        {
            m_buttonCharacter.interactable = false;
        }
    }

    private void CreateEffect()
    {
        if (!ProfilerModel.Instance.IsAbleCreateParticle.Value) return;
        ProfilerModel.Instance.IncreaseParticleCount();
        Debug.Log("CreateEffect()");

        var nowVec = Vector3.zero;

        switch (ProfilerModel.Instance.ParticleCount.Value)
        {
            case 1:
                nowVec = m_pivotScript.MM.localPosition;
                break;
            case 2:
                nowVec = m_pivotScript.LM.localPosition;
                break;
            case 3:
                nowVec = m_pivotScript.RM.localPosition;
                break;
            case 4:
                nowVec = m_pivotScript.LL.localPosition;
                break;
            case 5:
                nowVec = m_pivotScript.RR.localPosition;
                break;
        }

        var rnd = Random.Range(0, 100);
        var effectName = rnd % 2 == 0 ? "Effect001" : "Effect002";

        var effectPrefab = Resources.Load($"Prefab/Effects/{effectName}") as GameObject;
        var effect = Instantiate(effectPrefab) as GameObject;

        effect.transform.SetParent(m_transformParticle);
        effect.transform.localPosition = nowVec;
        effect.transform.rotation = Quaternion.identity;
        effect.transform.localScale = Vector3.one * 10;
        m_particletList.Add(effect);

        m_profilerScript.SetParticleCount(ProfilerModel.Instance.ParticleCount.Value);

        if (!ProfilerModel.Instance.IsAbleCreateParticle.Value)
        {
            m_buttonParticle.interactable = false;
        }
    }

    private void ChangeEmotion()
    {
        foreach(var character in m_characterList)
        {
            character.ChangeEmotion();
        }
    }

    private void DeleteAllObject()
    {
        if (m_characterList != null)
        {
            foreach (var character in m_characterList)
            {
                if (character == null) continue;
                Destroy(character.gameObject);
            }
            m_characterList.Clear();
        }

        if (m_particletList != null)
        {
            foreach (var particle in m_particletList)
            {
                Destroy(particle);
            }
            m_particletList.Clear();
        }
        ProfilerModel.Instance.SetCharacterCount(0);
        ProfilerModel.Instance.SetParticleCount(0);

        if (!m_buttonParticle.interactable) m_buttonParticle.interactable = true;
        if (!m_buttonCharacter.interactable) m_buttonCharacter.interactable = true;


    }

    private void OnDestroy()
    {
        Debug.Log("Presenter.Destroy()"); 

        DeleteAllObject();
        m_characterList = null;
        m_particletList = null;

        m_buttonParticle = null;
        m_buttonCharacter = null;
        m_buttonEmotion = null;
        m_buttonOnOff = null;
        m_profilerScript = null;
    }
}
