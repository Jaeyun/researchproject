﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class SceneScript : MonoBehaviour 
{
    [SerializeField] private Presenter m_presenter;
    [SerializeField] private Image m_BackGround;

    private void Awake()
    {
        Application.targetFrameRate = 60;
        if (m_BackGround == null) Debug.LogError("???");
        SetBackGround();
    }

    private void SetBackGround()
    {
        var rnd = Random.Range(0, 100);
        var tmpVal = rnd % 10;
        var image = 0;

        if (tmpVal >= 0 && tmpVal < 4) image = 3;
        else if (tmpVal >= 4 && tmpVal < 8) image = 2;
        else image = 1;
        var path = $"BackGround/BackImg{image.ToString()}";
        Debug.Log(path);

        Texture2D texture = Resources.Load(path) as Texture2D;
        m_BackGround.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
    }

    private void Start()
    {

        m_presenter.Init();
    }

    // Update is called once per frame
    void FixedUpdate () 
    {
        ProfilerModel.Instance.SetFPSVal(1f / Time.deltaTime);
    }

    private void OnDestroy()
    {
        Debug.Log("SceneScript.Destroy()");
        m_presenter = null;
        System.GC.Collect();
        Debug.Log("SceneScript.Destroy() Finished");
    }
}
